defmodule Crawler.FardaNews do
  use GenServer
  require Logger
  alias NewsData.News

  @farda_news_url "http://www.fardanews.com/"
  @rss_link [
    %{url: "http://www.fardanews.com/fa/rss/2", category: "سیاسی"},
    %{url: "http://www.fardanews.com/fa/rss/3", category: "فرهنگی"},
    %{url: "http://www.fardanews.com/fa/rss/5", category: "اقتصادی"},
  ]
  @timeout 12_000

  def start_link do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:fetch, state) do
    schedule_work()
    {:noreply, state}
  end

  defp schedule_work do
    Enum.map(@rss_link, &fetch_and_parse/1)
    Process.send_after(self(), :fetch, 4 * 60 * 1000) # fetch every 5 minutes
  end

  def fetch_and_parse(%{url: url, category: category} = _link) do
    with {:ok, %HTTPoison.Response{body: body, status_code: 200}} <- HTTPoison.get(url, [], [timeout: @timeout, recv_timeout: @timeout]),
         {:ok, feed, _} <- FeederEx.parse(body) do
           links = feed.entries |> Enum.take(5)
           res = Task.Supervisor.async_stream_nolink(CrawlerTask, links, __MODULE__, :extract_news, [category], max_concurrency: 3, timeout: @timeout)
           Enum.to_list(res)
         end
  end

  def extract_news(%FeederEx.Entry{link: link} = _entry, category) do
    Logger.info (inspect(self()) <> " " <> link)
    with {:ok, %HTTPoison.Response{body: body, status_code: 200}} <- HTTPoison.get(link, [], [timeout: 2_000, recv_timeout: 2_000, follow_redirect: true]),
         title = Floki.find(body, ".headline") |> Floki.text() |> String.trim(),
         sub_title = Floki.find(body, ".excerpt p") |> Floki.text() |> String.trim(),
         tags = Floki.find(body, ".keywords span a") |> Enum.map(&Floki.text/1),
         content = Floki.find(body, ".content") |> hd(),
         {_, _, x} = content do

      flat_content =
        x
        |> Enum.flat_map(&flatten/1)
        |> Enum.map(&extract_text_image/1)
        |> Enum.reject(fn x -> x in [" ", nil, ""] end)
      IO.inspect(flat_content)
      cs = News.changeset(%News{},
        %{link: link, title: title, tags: tags,
          sub_title: sub_title, category: category, source: "فردا نیوز",
          content: %{contents: flat_content}})
      res = NewsData.Repo.insert(cs)
      IO.inspect(res)
    else
      s ->
        Logger.error(inspect s)
        "Nothings happend"
    end
  end

  defp flatten({_, _, x}) do
    x
  end
  defp flatten(x) when is_binary(x) do
    [x]
  end

  defp extract_text_image(x) when is_binary(x) do
    case String.trim(x) do
      "" -> nil
      x ->
        %{
          "type": "text",
          "content": x
        }
    end
  end
  defp extract_text_image({"br", [], []}) do
  end
  defp extract_text_image(x) do
    case Floki.find(x, "img") do
      [] -> nil
      im ->
        src = Floki.attribute(im, "src") |> Floki.text()
        alt = Floki.attribute(im, "alt") |> Floki.text()
        src =
          case String.starts_with?(src, "http") do
            true ->
              src
            _ ->
              @farda_news_url <> src
          end
        %{
          "type": "image",
          "src": src,
          "alt": alt
        }
    end
  end
end
