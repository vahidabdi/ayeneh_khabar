defmodule Crawler.IribNews do
  use GenServer
  require Logger
  alias NewsData.News

  @rss_link [
    %{url: "http://www.iribnews.ir/fa/rss/4", tag: "ورزشی"},
    %{url: "http://www.iribnews.ir/fa/rss/5", tag: "سیاسی"},
    %{url: "http://www.iribnews.ir/fa/rss/6", tag: "اقتصادی"},
    %{url: "http://www.iribnews.ir/fa/rss/7", tag: "اجتماعی"},
  ]
  @timeout 12_000

  def start_link do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:fetch, state) do
    schedule_work()
    {:noreply, state}
  end

  defp schedule_work do
    Enum.map(@rss_link, &fetch_and_parse/1)
    Process.send_after(self(), :fetch, 5 * 60 * 1000) # fetch every 5 minutes
  end

  def fetch_and_parse(%{url: url, tag: tag} = _link) do
    with {:ok, %HTTPoison.Response{body: body, status_code: 200}} <- HTTPoison.get(url, [], [timeout: 20_000, recv_timeout: 20_000]),
         content = :zlib.gunzip(body),
         {:ok, feed, _} <- FeederEx.parse(content) do
           links = feed.entries |> Enum.take(5)
           res = Task.Supervisor.async_stream_nolink(CrawlerTask, links, __MODULE__, :extract_news, [tag], max_concurrency: 2, timeout: @timeout)
           Enum.to_list(res)
         end
  end

  def extract_news(%FeederEx.Entry{link: link} = _entry, tag) do
    with {:ok, %HTTPoison.Response{body: body, status_code: 200}} <- HTTPoison.get(link, [], [timeout: 20_000, recv_timeout: 20_000, follow_redirect: true]),
         content = :zlib.gunzip(body),
         title = Floki.find(content, ".title h1 span") |> Floki.text() |> String.strip(),
         subtitle = Floki.find(content, ".subtitle") |> Floki.text() |> String.strip(),
         news_content = Floki.find(content, ".body.body_media_content_show") |> Floki.text() |> String.strip(),
         tags = Floki.find(content, ".tags_title a") |> Enum.map(&(Floki.text(&1))) do
       cs = News.changeset(%News{},
         %{link: link, title: title, subtitle: subtitle, tags: [tag],
           source: "iribnews", content: %{p: news_content}})
        NewsData.Repo.insert(cs)
        "done"
    else
        s ->
          Logger.error(s)
          "Nothings happened"
    end
  end
end
