defmodule Crawler.Tabnak do
  use GenServer
  require Logger
  alias NewsData.News

  @rss_link [
    %{url: "http://www.tabnak.ir/fa/rss/6", tag: "اقتصادی"},
    %{url: "http://www.tabnak.ir/fa/rss/2", tag: "ورزشی"},
    %{url: "http://www.tabnak.ir/fa/rss/3", tag: "اجتماعی"},
  ]
  @timeout 12_000

  def start_link do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:fetch, state) do
    schedule_work()
    {:noreply, state}
  end

  defp schedule_work do
    Enum.map(@rss_link, &fetch_and_parse/1)
    Process.send_after(self(), :fetch, 5 * 60 * 1000) # fetch every 5 minutes
  end

  def fetch_and_parse(%{url: url, tag: tag} = _link) do
    with {:ok, %HTTPoison.Response{body: body, status_code: 200}} <- HTTPoison.get(url, [], [timeout: 2_000, recv_timeout: 2_000]),
         {:ok, feed, _} <- FeederEx.parse(body) do
           links = feed.entries |> Enum.take(5)
           res = Task.Supervisor.async_stream_nolink(CrawlerTask, links, __MODULE__, :extract_news, [tag], max_concurrency: 2, timeout: @timeout)
           x = Enum.to_list(res)
           IO.inspect(x)
         end
  end

  def extract_news(%FeederEx.Entry{link: link} = _entry, tag) do
    with {:ok, %HTTPoison.Response{body: body, status_code: 200}} <- HTTPoison.get(link, [], [timeout: 2_000, recv_timeout: 2_000, follow_redirect: true]),
         content = :zlib.gunzip(body),
         title = Floki.find(content, "h1.Htag a") |> Floki.text() |> String.strip(),
         subtitle = Floki.find(content, ".subtitle") |> Floki.text() |> String.strip(),
         news_content = Floki.find(content, ".body") |> Floki.text() |> String.strip() do
      cs = News.changeset(%News{},
        %{link: link, title: title, tags: [tag],
          source: "tabnak", content: %{p: news_content}})
      NewsData.Repo.insert(cs)
      "done"
    else
      s ->
        Logger.error(inspect s)
        "Nothings happend"
    end
  end
end
