const webpack = require("webpack");
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  entry: [
    './css/app.scss',
    './js/app.js',
  ],
  output: {
    path: path.resolve(__dirname, '../priv/static'),
    filename: 'js/app.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'style-loader',
            'css-loader',
            'postcss-loader'
          ]
        })
      },
      {
        test: /\.(scss|sass)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            'postcss-loader',
            {
              loader: 'sass-loader',
              options: {
                includePaths: [__dirname + '/node_modules']
              }
            }
          ]
        })
      },
      // {
      //   test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
      //   use: {
      //     loader: 'file-loader?name=fonts/[name].[ext]',
      //     options: {
      //       limit: '10000',
      //       mimetype: 'application/font-woff'
      //     }
      //   }
      // },
      // {
      //   test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      //   use: {
      //     loader: 'file-loader?name=fonts/[name].[ext]',
      //     options: {
      //       limit: '10000',
      //       mimetype: 'application/octet-stream'
      //     }
      //   }
      // },
      {
        test: /\.(eot|svg|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        use: 'file-loader?name=/fonts/[hash].[ext]'
      },
      // {
      //   test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      //   use: {
      //     loader: 'file-loader?name=fonts/[name].[ext]',
      //     options: {
      //       limit: '10000',
      //       mimetype: 'img/svg+xml'
      //     }
      //   }
      // },
      {
        test: /\.(png|jpg|gif)$/,
        use: 'file-loader?name=/images/[hash].[ext]',
      }
    ],
  },
  plugins: [
    new ExtractTextPlugin('css/app.css'),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    })
  ],
  resolve: {
    extensions: ['.scss', '.css', '.js'],
  }
}
