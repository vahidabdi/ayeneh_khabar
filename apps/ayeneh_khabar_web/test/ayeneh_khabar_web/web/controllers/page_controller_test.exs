defmodule AyenehKhabarWeb.Web.PageControllerTest do
  use AyenehKhabarWeb.Web.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "سایت خبری آیینه خبر"
  end
end
