# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ayeneh_khabar_web,
  namespace: AyenehKhabarWeb

# Configures the endpoint
config :ayeneh_khabar_web, AyenehKhabarWeb.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "rEk4orVE3U88yaP5FDHp5Bk7yHNbQKdiQW1PAReL7xx0XCTNe0RyGdEVA+ASP+f7",
  render_errors: [view: AyenehKhabarWeb.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AyenehKhabarWeb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
