defmodule AyenehKhabarWeb.Web.PageController do
  use AyenehKhabarWeb.Web, :controller
  import Ecto.Query

  def index(conn, _params) do
    slider_query = from q in NewsData.News, where: (fragment("content->'contents' @> ANY (ARRAY['[{\"type\": \"image\"}]']::jsonb[])")), order_by: [desc: q.inserted_at], limit: 3
    query = from q in NewsData.News, order_by: [desc: q.inserted_at], limit: 20
    politics = from q in NewsData.News, where: q.category == "سیاسی", order_by: [desc: q.inserted_at], limit: 10
    culture = from q in NewsData.News, where: q.category == "فرهنگی", order_by: [desc: q.inserted_at], limit: 10
    business = from q in NewsData.News, where: q.category == "اقتصادی", order_by: [desc: q.inserted_at], limit: 10

    slider_news = NewsData.Repo.all(slider_query)
    all_news = NewsData.Repo.all(query)
    politics_news = NewsData.Repo.all(politics)
    cultures_news = NewsData.Repo.all(culture)
    business_news = NewsData.Repo.all(business)

    render conn, "index.html", all_news: all_news, slider_news: slider_news,
                               politics_news: politics_news, cultures_news: cultures_news,
                               business_news: business_news
  end

  def show(conn, %{"news_id" => news_id} = _params) do
    news = NewsData.Repo.get(NewsData.News, news_id)
    render conn, "show.html", news: news
  end
end
