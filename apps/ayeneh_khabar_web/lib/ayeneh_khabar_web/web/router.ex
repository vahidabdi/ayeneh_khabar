defmodule AyenehKhabarWeb.Web.Router do
  use AyenehKhabarWeb.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug AyenehKhabarWeb.Web.Plug.CurrentDate
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AyenehKhabarWeb.Web do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/news/detail/:news_id", PageController, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", AyenehKhabarWeb.Web do
  #   pipe_through :api
  # end
end
