defmodule AyenehKhabarWeb.Web.Plug.CurrentDate do
  import Plug.Conn


  def init(options) do
    options
  end

  def call(conn, _opts) do
    today = Jalaali.to_jalaali Date.utc_today

    conn
    |> assign(:current_date, "#{today}")
  end
end
