defmodule NewsData.Repo.Migrations.News do
  use Ecto.Migration

  def up do
    execute "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\" WITH SCHEMA public;"
    create table(:news, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :link, :text, null: false
      add :title, :text, null: false
      add :sub_title, :text
      add :category, :text
      add :source, :text, null: false
      add :content, :map, null: false
      add :tags, {:array, :text}, null: false
      add :meta, :map
      add :published_at, :timestamptz
      add :inserted_at, :timestamptz
      add :updated_at, :timestamptz
    end

    create unique_index(:news, :link)
    create index(:news, [:tags], using: :gin)
    create index(:news, [:inserted_at])
  end

  def down do
    drop table(:news)
  end
end
