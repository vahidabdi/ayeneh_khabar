defmodule NewsData.News do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}

  schema "news" do
    field :link, :string
    field :title, :string
    field :sub_title, :string
    field :category, :string
    field :source, :string
    field :content, :map
    field :tags, {:array, :string}
    field :meta, :map
    field :published_at

    timestamps()
  end

  def changeset(news, params \\ %{}) do
    news
    |> cast(params, [:link, :title, :sub_title, :category, :source,
                     :content, :tags, :meta, :published_at])
    |> validate_required([:link, :title, :source, :content, :category])
    |> unique_constraint(:link)
  end
end
